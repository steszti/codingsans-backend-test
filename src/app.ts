import express, { Application, Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import morgan from 'morgan';
import bcrypt from 'bcrypt';
import mongodb from 'mongodb';
import fetch from 'node-fetch';
import bodyparser from 'body-parser';

import { config } from './config';

export const startApp = async (): Promise<void> => {
  // create application
  // start application
  const app: Application = express();
  const port: string = process.env.PORT || '3000';
  app.use(morgan('combined'));
  app.use(express.json({ limit: '50mb' }));
  app.use(bodyparser.json());

  type User = {
    id: string;
    username: string;
    password: string;
  };

  const users: User[] = [];

  const uri = config.DATABASE_URL;
  const MongoClient = mongodb.MongoClient;
  const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
  await client
    .connect()
    .then(() => {
      const collection = client.db('Users').collection('test_users');
      collection.find({}).toArray(function(err: Error, result: User[]) {
        if (err) {
          throw err;
        }
        users.push(...result);
        client.close().catch(err => {
          throw err;
        });
        return console.log('Successfully connected to database');
      });
    })
    .catch(error => {
      console.log('Error connecting to database: ', error);
      return process.exit(1);
    });

  app.post('/login', async (req: Request, res: Response) => {
    const user = users.find(user => user.username === req.body.username);
    if (user) {
      if (await bcrypt.compare(req.body.password, user.password)) {
        const tokenSecret = config.ACCESS_TOKEN_SECRET;
        const accessToken = jwt.sign(user, tokenSecret, { expiresIn: '2h' });
        return res.send(`Successful login, token: ${accessToken} `);
      } else {
        res.status(401).send('Wrong password');
        return;
      }
    }
    return res.status(401).send('Invalid username');
  });

  app.get('/breweries', async (req: Request, res: Response) => {
    const token = req.headers['authorization'] as string;
    if (token) {
      try {
        const tokenSecret = config.ACCESS_TOKEN_SECRET;
        jwt.verify(token, tokenSecret);
        if (req.query.query) {
          const url = 'https://api.openbrewerydb.org/breweries/search?';
          const params = new URLSearchParams();
          params.append('query', req.query.query.toString());
          await fetch(url + params)
            .then(response => response.json())
            .then(data => {
              return res.send(data);
            })
            .catch(err => {
              throw err;
            });
          return;
        } else {
          const url = 'https://api.openbrewerydb.org/breweries';
          await fetch(url)
            .then(response => response.json())
            .then(data => {
              return res.send(data);
            })
            .catch(err => {
              throw err;
            });
          return;
        }
      } catch (err) {
        return res.status(401).send('Invalid token');
      }
    } else {
      return res.status(401).send('Token is missing');
    }
  });

  app.use(async (req: Request, res: Response, next: NextFunction) => {
    if (req.body !== 'breweries' || req.body !== 'login') {
      return res.status(404).send('Not found');
    } else {
      return next();
    }
  });

  app.listen(port, () => {
    console.log(`Server running on ${port}`);
  });
};
