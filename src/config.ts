import dotenv from 'dotenv';
dotenv.config();

export const config = {
  ACCESS_TOKEN_SECRET: process.env.ACCESS_TOKEN_SECRET || '',
  DATABASE_URL: process.env.DATABASE_URL || '',
};
